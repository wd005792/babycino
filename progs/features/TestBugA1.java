class TestBugA1{
  public static void main(String[] a) {
    System.out.println(new Test().f());
  }
}

class Test{
  public int f(){
    int num;
    boolean finish;
		num = 1;
		finish = false;
		while (finish == num){
      num = 0;
      finish = true;
    }
    return num;
  }
}
